<NotepadPlus>
    <Project name="VpnViewer">
        <Folder name="bin">
            <Folder name="iasviewern_v288">
                <Folder name="Sample">
                    <File name="bin\iasviewern_v288\Sample\s2.log" />
                    <File name="bin\iasviewern_v288\Sample\sample.log" />
                </Folder>
                <File name="bin\iasviewern_v288\ag_template.rpt" />
                <File name="bin\iasviewern_v288\a_dnary.xml" />
                <File name="bin\iasviewern_v288\br_template.rpt" />
                <File name="bin\iasviewern_v288\eventmsg.dll" />
                <File name="bin\iasviewern_v288\history.txt" />
                <File name="bin\iasviewern_v288\iasviewer.chm" />
                <File name="bin\iasviewern_v288\IASViewer.exe" />
                <File name="bin\iasviewern_v288\license.txt" />
                <File name="bin\iasviewern_v288\readme.txt" />
            </Folder>
        </Folder>
        <Folder name="lib">
            <Folder name="vpnviewer">
                <File name="lib\vpnviewer\cmd_parser.rb" />
                <File name="lib\vpnviewer\log.rb" />
                <File name="lib\vpnviewer\app.rb" />
            </Folder>
            <File name="lib\vpnviewer.rb" />
            <Folder name="html">
                <File name="lib\html\index.html" />
                <File name="lib\html\main.js" />
                <File name="lib\html\jquery.js" />
            </Folder>
        </Folder>
        <File name="readme.md" />
    </Project>
</NotepadPlus>
