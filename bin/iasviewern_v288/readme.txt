IAS Log Viewer version 2.88
===========================


OVERVIEW
--------
  IAS Log Viewer program reads and interprets the log files from
 Windows 2000/2003/2008/2012 Routing and Remote Access Service. 
 With IAS Log Viewer you can view log file in a user-friendly form.


INSTALLATION
------------
  Unzip iasviewer.zip and run setup.exe to walk through installation
 wizard. 


Deinstalling IAS Log Viewer
--------------
 Use "Control panel"->"Add/Remove program" to uninstall IAS Log Viewer.


TRIAL VERSION
--------------
 The trial version 2.88 of IAS Log Viewer will expire 15 days from installation
date. After expiration a license key is required.


REGISTRATION
------------
  IAS Log Viewer is a shareware product.  If you find it useful and want to receive the latest versions please
 register your copy. Registration is available online at
 http://www.shareit.com/programs/167885.htm. Alternatively, you can
 go to http://www.shareit.com and enter the program number there: 150653.
 

  Advantages of registering IAS Log Viewer include:

  - You will receive the registration code that removes nag screen.
  - You will be notified about new versions of IAS Log Viewer.
  - You will receive new versions during subscription period for FREE. 


INTERNET LINKS
--------------
  http://www.iaslog.com/index.html - IAS Log Viewer home page.


AUTHOR 
------
   Andrei Roofin, DeepSoftware 
   e-mail: ray@deepsoftware.com
      www: http://www.deepsoftware.com/
