﻿require_relative 'vpnviewer/cmd_parser'
require_relative 'vpnviewer/app'

module VpnViewer
  VER = '0.1'
  RUBY_VER = '1.9.3p545 (2014-02-24) [i386-mingw32]'
end

if __FILE__ == $0
  puts "Started"
  parser = VpnViewer::CmdParser.new(ARGV)
  parser.check
  app = VpnViewer::App.new(parser)
  app.start
  puts "Completed"
end