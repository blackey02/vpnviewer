﻿require 'date'

module VpnViewer
  class Log
    attr_reader :path
    attr_reader :date
    
    def initialize(path)
      @path = path
      @date = get_date
    end
    
    private
    
    # Parses U.S. style date
    def get_date
      line = File.open(@path, &:readline)
      dates = line.split(',')
      date_str = dates[2]
      date_parts = date_str.split('/')
      month = date_parts[0]
      day = date_parts[1]
      year = date_parts[2]
      Date.new(year.to_i, month.to_i, day.to_i)
    end 
  end
end