﻿require 'optparse'
require 'ostruct'

module VpnViewer
  class CmdParser
    attr_reader :options
    @args = nil
    @options = nil
    @opt_parser = nil
    
    def initialize(args)
      @args = args
      @options = OpenStruct.new
      @options.log_dir = nil
      
      @opt_parser = OptionParser.new do |opts|
        opts.banner = "Usage: ruby vpnviewer.rb [options]"
        opts.separator ""
        opts.separator "Options:"

        opts.on("-l dir", "--log_dir", "Required: Directory of Microsoft Windows 2k3 VPN log files") do |dir|
          @options.log_dir = dir.gsub!('\\', '/')
        end
        
        opts.on_tail("-h", "--help", "Show this message") do
          puts opts
          exit
        end
        
        opts.on_tail("-v", "--version", "Show version") do
          puts "Application Version: #{VER}"
          puts "Ruby Version: #{RUBY_VER}"
          exit
        end
      end
    end
    
    def check
      @opt_parser.parse!(@args)
      if @options.log_dir.nil?
        puts "\nMissing Argument: Enter \'ruby vpnviewer.rb -h' for help\n"
        exit(1)
      end
    end
  end
end