﻿require 'find'
require 'fileutils'
require_relative 'log'

module VpnViewer
  class App
    @parser = nil
    @logs = nil
  
    def initialize(parser)
      @logs = Array.new
      @parser = parser
    end
    
    def start
      get_logs
      process_logs
    end
    
    private
    
    def get_logs
      @logs.clear
      Find.find(@parser.options.log_dir).each do |path|
        if path =~ /.*\.log$/
          log = VpnViewer::Log.new(path)
          @logs << log
        end
      end
    end
    
    def process_logs
      root_dir = get_create_root_dir
      dates_found = Hash.new()
      
      @logs.each do |log|
        year = log.date.year
        month = log.date.month
        destination = File.join(root_dir, year.to_s, month.to_s)
        if !Dir.exists?(destination) then FileUtils.mkdir_p(destination) end
        run_report(log, destination)
        dates_found[year] = Array.new() if !dates_found.has_key?(year)
        dates_found[year] << month
      end
      copy_html(root_dir, dates_found)
    end
    
    def copy_html(destination, dates_found)
      FileUtils.cp_r(Dir[File.join(File.dirname(__FILE__), "../html/*")], destination)
      main_js = update_main_js(dates_found)
      index_html = update_index_html(dates_found)
      IO.write(File.join(destination, "index.html"), index_html)
      IO.write(File.join(destination, "main.js"), main_js)
    end
    
    def get_create_root_dir
      requested_dir = File.join(Dir.home, "Desktop", "VpnViewerResults")
      if Dir.exists?(requested_dir) then FileUtils.rm_r(requested_dir) end
      sleep(1) while Dir.exists?(requested_dir)
      Dir.mkdir (requested_dir)
      requested_dir
    end
    
    def run_report(log, destination)
      cmd = "\"" + File.join(File.dirname(__FILE__), "../../bin/", "iasviewern_v288", "IASViewer.exe") + "\""
      cmd << " -i\"#{log.path}\""
      cmd << " -rd\"#{destination}\""
      cmd << " -rthtml"
      cmd << " -rpalldata"
      cmd << " -rru;ud;rj;cc;ag"
      cmd.gsub!('/', '\\')
      system(cmd)
    end
    
    # Updates the index.html template with actual data
    def update_index_html(dates_found)
      data = ""
      dates_found.each_key do |year|
        data << "<a href=\"javascript:yearSelected('#{year}');\">#{year}</a><br />\n"
      end
      main_js = IO.read(File.join(File.dirname(__FILE__), "../html/index.html"))
      main_js.sub("RUBY_CODE", data)
    end
    
    # Updates the javascript main.js template with actual data
    def update_main_js(dates_found)
      data = ""
      dates_found.each do |year,month_array|
        data << "if(year == '#{year}') {"
        month_array.each do |month|
          data << "months.push('<a href=\"javascript:monthSelected(#{month})\">#{Date::MONTHNAMES[month]}</a><br />');\n"
        end
        data << "}"
      end
      main_js = IO.read(File.join(File.dirname(__FILE__), "../html/main.js"))
      main_js.sub("RUBY_CODE", data)
    end
     
  end
end







