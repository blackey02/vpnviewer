VPNViewer
====================

Summary
---------------------
The VPNViewer is used to generate a manageable report from multiple log files generated monthly by a Windows 2003 VPN using U.S. dates.

Features
---------------------
* Reads the log files and generates a list of years and months covered by all logs
* Produces a single index.html that allows you to easily navigate by year and month

Building
---------------------
1. git clone https://bitbucket.org/blackey02/vpnviewer.git
2. Download and install Ruby >= version 1.9.3 if not installed
3. ruby vpnviewer.rb -l "PATH_TO_LOGS_DIR"

Screens
---------------------
![](https://copy.com/Fm0B9uquSdIV)
![](https://copy.com/pjceUao1a0LW)
![](https://copy.com/AV7tYSfxoHMq)

Acknowledgements
---------------------
* [IAS Log Viewer](http://www.deepsoftware.com/iasviewer/)